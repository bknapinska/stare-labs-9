package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = Teram_Imp.class;
    public static Class<? extends Consultation> consultationBean = Consultation_Impl.class;
    public static Class<? extends ConsultationList> consultationListBean = ContultationList_Imp.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationListFactory_Impl.class;
   
    
}
