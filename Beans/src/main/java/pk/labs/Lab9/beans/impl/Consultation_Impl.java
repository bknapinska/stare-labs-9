/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class Consultation_Impl implements Consultation {
    public String student;
    public Date bd;
    public Date ed;
    
    public String getStudent()
    {
        return this.student;
    }
    
    public void setStudent(String student)
    {
        System.out.print("c");
    }

    public Date getBeginDate()
    {
        return this.bd;
    }

    public Date getEndDate()
    {
        return this.ed;
    }
    
    public void setTerm(Term term) throws PropertyVetoException
    {
        System.out.print("d");
    }
    
    public void prolong(int minutes) throws PropertyVetoException
    {
        System.out.print("e");
    }
}
